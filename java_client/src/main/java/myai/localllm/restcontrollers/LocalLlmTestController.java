package myai.localllm.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import myai.localllm.client.LocalLlmClient;

/**
 * A RESTful controller for testing the Local LLM Client using Swagger UI.
 * Swagger UI URL - http://localhost:8080/swagger-ui.html
 */
@RestController
@RequestMapping("/localllm")
@Api(value = "Test local LLM with Java client")
public class LocalLlmTestController {
	@Autowired
	LocalLlmClient client;

	@ApiOperation(value = "prompt response", response = String.class)
	@RequestMapping(value = "/chat", method = RequestMethod.POST)
	public String getHealth(String prompt) {
		String response = client.query(prompt);
		return response;
	}
}
