package myai.localllm.client;

/**
 * This class is a Java client that and communicate the Local Language Model (LLM) Server included in this project.  
 */
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class LocalLlmClient {
	private RestTemplate restTemplate;
	private String url;
	private HttpHeaders headers;

	/**
	 * Constructor
	 */
	public LocalLlmClient(String url, String apiKey) {
		this.restTemplate = new RestTemplate();
		this.headers = new HttpHeaders();
		this.url = url;
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + apiKey);
	}

	/**
	 * This method makes a call to the local LLM RESTful API. Example prompt:
	 * "Perform one task based on the following objective: Solve world hunger. Your task: Develop a task list. Response:"
	 * 
	 * @param prompt LLM prompt sent to the local LLM API
	 * @return Response received from the local LLM API
	 */
	public String query(String prompt) {
		JSONObject payload = new JSONObject();
		payload.put("prompt", prompt);
		HttpEntity<String> request = new HttpEntity<String>(payload.toString(), headers);
		return restTemplate.postForObject(url, request, String.class);
	}
}
