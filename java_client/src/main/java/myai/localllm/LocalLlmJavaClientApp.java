package myai.localllm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
/**
 * This Spring Boot application serves as an example of using a Java client
 * to interact with a Local Language Model (LLM) Server.
 */
@SpringBootApplication
@ComponentScan("myai.localllm")
public class LocalLlmJavaClientApp {
	private static final Logger logger = LoggerFactory.getLogger(LocalLlmJavaClientApp.class);

	public static void main(String[] args) {
		SpringApplication.run(LocalLlmJavaClientApp.class, args);
		logger.info("Local LLM Client App is running...");
	}
}
