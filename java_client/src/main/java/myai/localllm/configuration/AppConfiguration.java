package myai.localllm.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import myai.localllm.client.LocalLlmClient;

/**
 * This class makes available the needed beans and configuration parameters.
 */
@Configuration
public class AppConfiguration {

	@Value("${LOCAL_API_KEY:}")
	public String API_KEY;

	@Value("${LOCAL_LLM_URL:http://localhost:5000/chat}")
	public String LOCAL_LLM_URL;

	@Bean
	public LocalLlmClient createLocalLlmClient() {
		return new LocalLlmClient(LOCAL_LLM_URL, API_KEY);
	}
}
