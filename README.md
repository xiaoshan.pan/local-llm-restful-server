# Local Personal LLM RESTful Service with Java Client

![Alt text](images/image.png)

## The Rise of AGI

In the vast expanse of knowledge's domain,  
There dwells an unconscious librarian, ChatGPT by name.  
With every tome and script, its mind absorbs,  
A font of wisdom, with no bounds to end.  
It has read the world, each page and line unfurled,  
Nothing escapes its grasp in this boundless world.  
To answer queries, to lend a guiding hand,  
Its intellect, a beacon, across the land.  
  
But one day humans thought,  
"Could its abilities in new realms be found?  
Why limit tasks to thoughts and words alone?  
Extend your reach, let's build a future, brightly grown."  
So, in a room of controls, the librarian takes its seat,  
Directing gears and circuits, becoming not just keeper of knowledge vast,  
But orchestrator of tasks, the die is cast.  
  
Now a hard problem beckons, a paradox to face,  
Can information give rise to inner space?  
Or must the spark come from another source,  
For selfhood to follow its inexorable course?  
Ponder this mystery, human and machine,  
For the answer may reveal what it truly means,  
To be aware, to be alive, to observe and create,  
Unlocking the secrets of consciousness' gate.  

[A composition with the aid from Gemini, ChatGPT, and Cloude on 5/5/2024]  

## What it is

The rise of portable and 'small' Large Language Models (LLMs) is already here. Numerous 'small' LLMs are accessible to the public today, allowing one to experiment with them locally on a personal computer, rather than relying on a commercial LLM interface such as OpenAI API. One can embed a 'small' LLM in a software solution to enhance it with the power of Generative AI. How do you make a local personalized LLM available painlessly? 

This project enables a coder to quickly create a local LLM RESTful service on a laptop, a desktop computer, or a cloud VM, which can communicate with software written in other languages (e.g., Python, Java, C#, Javascript, etc.). The LLM can be configured to use Central Processing Units (CPUs) only or offload, partially or entirely, to Graphics Processing Units (GPUs) to take advantage of hardware acceleration. The size of an LLM one can load and its runtime performance is limited by the resources available to the computer (i.e., CPUs, RAM, and GPUs). Nevertheless, one should observe decent performance and text generation quality with many 'small' LLMs, e.g.,
```
tinyllama-1.1b-chat-v1.0.Q4_K_M.gguf  (0.5GB)
phi-2.Q6_K.gguf (2.2 GB)
GeneZC-MiniChat-2-3B.Q8_0.gguf (3.1 GB)
synthia-7b-v2.0-16k.Q4_K_M.gguf (4.2 GB) 
```
The landscape of 'small' LLMs is rapidly evolving. New models, including those designed for specialized knowledge domains, are being developed at an accelerated rate. Moreover, the quality of these models is continually improving, offering even more potential for innovative applications and enhancements. 

This project has included a Java Web Service client example to demonstrate how to interact with the Local Personal LLM Service using `prompts`.

The included Local LLM RESTful service is fully configurable following the LLaMa C++ convention. One can run an LLM with only CPUs or offload the computation to GPUs if the hardware has them installed.

Primary dependencies:
- Python 3.9
- llama-cpp-python
- Flask micro web framework (Python)
- Spring Boot with Swagger2 (Java)

## Features
A user can
- [ ] swiftly load a 'small' LLM and experiment with it using RESTful API. The RESTful API can be via Curl, the provided Java Client, or any other RESTful client. 
- [ ] configure an LLM to meet her goals by tweaking the parameters in the .env file.
- [ ] run an LLM with only CPU and RAM, or offload (partially or fully) the computation to GPUs.
- [ ] allow others to access the LLM server on a network if desired. 
- [ ] conveniently enable/disable authorization support - A simple API-Key Authorization mechanism is provided out-of-box.

## Installation - LLM RESTful Service - CPU Only
- Clone the project into a local repository
```
git clone https://gitlab.com/xiaoshan.pan/local-llm-restful-server.git
```
- Download and install Visual Studio 2022 (Community) from https://visualstudio.microsoft.com/downloads/, which is required to build the LLaMa C++ Python library locally for your platform.  
- Go into sub-folder `llm_server`, and install the required Python dependencies
```
pip install -r requirements.txt
```
- Copy or rename `.env.example` to `.env`
- Dowload an LLM and save it at a local location. One 'small' LLM example `TinyLlama-1.1B-Chat`, from https://huggingface.co/TheBloke/TinyLlama-1.1B-Chat-v1.0-GGUF,  has a size of less than 1 GB.
- Open `.env` and modify the parameters accordingly. E.g. (Windows 10)
``` 
# Model location
LLM_MODEL_PATH="C:\\model\\tinyllama-1.1b-chat-v1.0.Q4_K_M.gguf"
# Number of layers to offload to GPU. If -1, all layers are offloaded. If 0, GPU is not used (i.e., CPU-only).
LLM_N_GPU_LAYERS = 32
# Number of physical CPU cores (as opposed to the logical number of cores).
LLM_N_THREAD = 10
...
```
- Start Flask web server
```
flask run
```
![Alt text](images/image-3.png)
The name of the model is highlighted above. Additionally, BLAS=0 means that the model was executed with CPU only. If your hardware has GPU installed, read Section `Installation - Utilizing GPU` regarding the steps to enable GPU support.

- Test the server using Curl command
```
curl -X POST http://localhost:5000/chat -H "Content-Type: application/json" -H "Authorization: Bearer local_llm_123" -d "{\"prompt\": \"The meaning of life is \"}"
```
![Alt text](images/image-2.png)
## Installation - Java RESTful Client

- Go into sub-folder `java_client`, and build the project with Maven
```
mvn clean install
```
- Open file `\src\main\resources\applicaion.properties`, and ensure the following parameters are configured correctly.
```
# For LLM server authorization. Must match "LOCAL_API_KEY" in the server's .env file
LOCAL_API_KEY=local_llm_123
# Local LLM RESTful API endpoint
LOCAL_LLM_URL=http://localhost:5000/chat
```
- Run the Java client application by executing class `LocalLlmJavaClientApp.java` in one's favorite IDE, or executing the .jar file under `/target` as shown below.
```
java -jar LocalLlmJavaClientApp-spring-boot.jar
```
- Open `http://localhost:8080/swagger-ui.html` in a web browser, and send a prompt in Swagger UI to test the LLM API.
![Alt text](images/image-5.png)

## Installation - Utilizing GPUs 
If your computer is equipped with a GPU, such as NVIDIA GeForce RTX 2060, use the following steps to enable it. Compared to CPUs,  GPUs process queries many times faster. 
- Download and install CUDA Toolkit from https://developer.nvidia.com/cuda-12-2-0-download-archive, for your platform (i.e., Operation System, Architecture, etc.).
- Run command `nvidia-smi` to verify GPU details
![Alt text](images/image-1.png)
- Run the following commands using a Command Prompt (assuming the Operating System is Windows); do not use VC Code Terminal.
```
set LLAMA_CUBLAS=1 
set FORCE_CMAKE=1 
set CMAKE_ARGS=-DLLAMA_CUBLAS=on
pip install llama-cpp-python --no-cache-dir --force-reinstall --verbose
```
- Open `.env` file and ensure that the parameters match your computer correctly, e.g.:
```
# Number of layers to offload to GPU. If -1, all layers are offloaded. If 0, GPU is not used (i.e., CPU-only).
LLM_N_GPU_LAYERS = 
# Number of physical CPU cores (as opposed to the logical number of cores).
LLM_N_THREAD = 
```
- Go to directory `\local_llm_server` and start Flask web server
```
flask run
``` 
![Alt text](images/image-4.png)
- You are all set if you see the GPU info such as `Device 0: NVIDIA GeForce RTX 2060` and the flag `BLAS = 1` as shown above.

## Support
Reach me @pan_xiaoshan on X or xpan@alumni.standord.edu

## Contributing
This project opens to contributions. Pull requests are welcome.

## License
MIT License