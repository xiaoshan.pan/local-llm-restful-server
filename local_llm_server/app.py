# This Python code loads a Language Model (LLM) from a local disk location and exposes it as a RESTful service.
# It utilizes GPUs if available; otherwise, it gracefully falls back to using CPU and RAM.
# For a comprehensive list of model configuration options, refer to the ".env.example" file.
# Additionally, the code supports a simple API_Key-based authorization mechanism.

from llama_cpp import llama_log_set, Llama
from dotenv import load_dotenv
import ctypes, os
from flask import Flask, request, abort
from timeit import default_timer as timer


# Disable llama.cpp verbose output
def my_log_callback(level, message, user_data):
    pass


log_callback = ctypes.CFUNCTYPE(None, ctypes.c_int, ctypes.c_char_p, ctypes.c_void_p)(
    my_log_callback
)
llama_log_set(log_callback, ctypes.c_void_p())

# Load default environment variables (.env)
load_dotenv()
m_params = {
    "LLM_MODEL_PATH": os.getenv("LLM_MODEL_PATH"),
    "LLM_CONTEXT_SIZE": int(os.getenv("LLM_CONTEXT_SIZE")),
    "LLM_N_BATCH": int(os.getenv("LLM_N_BATCH")),
    "LLM_N_GPU_LAYERS": int(os.getenv("LLM_N_GPU_LAYERS")),
    "LLM_N_THREAD": int(os.getenv("LLM_N_THREAD")),
    "LLM_USE_MLOCK": os.getenv("LLM_USE_MLOCK") == "True",
    "LLM_NUMA": os.getenv("LLM_NUMA") == "True",
    "LLM_VERBOSE": os.getenv("LLM_VERBOSE") == "True",
    "LLAMA_MAX_TOKENS": int(os.getenv("LLAMA_MAX_TOKENS")),
    "LLAMA_TEMPERATURE": float(os.getenv("LLAMA_TEMPERATURE")),
    "LLAMA_TOP_P": float(os.getenv("LLAMA_TOP_P")),
    "LLAMA_TOP_K": int(os.getenv("LLAMA_TOP_K")),
    "LLAMA_REPEAT_PENALTY": float(os.getenv("LLAMA_REPEAT_PENALTY")),
    "LLAMA_ECHO": os.getenv("LLAMA_ECHO") == "True",
    "LOCAL_API_KEY": os.getenv("LOCAL_API_KEY", ""),
}

# Print all parameter values (for debug purposes)
# for name in m_params:
#     print(name + ":", m_params[name])

# Perform authorization or not
authorization = m_params["LOCAL_API_KEY"] != ""

# Load model
llm_model = Llama(
    model_path=m_params["LLM_MODEL_PATH"],
    n_ctx=m_params["LLM_CONTEXT_SIZE"],
    n_batch=m_params["LLM_N_BATCH"],
    n_gpu_layers=m_params["LLM_N_GPU_LAYERS"],
    n_threads=m_params["LLM_N_THREAD"],
    use_mlock=m_params["LLM_USE_MLOCK"],
    numa=m_params["LLM_NUMA"],
    verbose=m_params["LLM_VERBOSE"],
)


# Define generation function with query parameters
def generate_text_from_prompt(
    user_prompt,
    max_tokens=m_params["LLAMA_MAX_TOKENS"],
    temperature=m_params["LLAMA_TEMPERATURE"],
    top_p=m_params["LLAMA_TOP_P"],
    top_k=m_params["LLAMA_TOP_K"],
    repeat_penalty=m_params["LLAMA_REPEAT_PENALTY"],
    echo=m_params["LLAMA_ECHO"],
):
    model_output = llm_model(
        user_prompt,
        max_tokens=max_tokens,
        temperature=temperature,
        top_p=top_p,
        top_k=top_k,
        repeat_penalty=repeat_penalty,
        echo=echo,
    )
    final_result = model_output["choices"][0]["text"].strip()
    return final_result


## WEB Server ##
app = Flask(__name__)


# POST RESTful API that can be tested with curl, e.g.,
# curl -X POST http://localhost:5000/chat -H "Content-Type: application/json" -H "Authorization: Bearer local_llm_123" -d "{\"prompt\": \"The meaning of life is \"}"
@app.post("/chat")
def chat():
    # Authrization
    if authorization:
        if (
            not request.headers["Authorization"]
            == "Bearer " + m_params["LOCAL_API_KEY"]
        ):
            abort(401)
    # Request body needs to be {"prompt": "bla"}
    if not request.json or not "prompt" in request.json:
        abort(400)
    start = timer()
    response = generate_text_from_prompt(request.json["prompt"])
    print("Took ", timer() - start)
    return response, 201
